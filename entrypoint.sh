#!/bin/sh -e

#
# original file supplied with the wernight/docker-ngrok docker container
# has been adapted to use multple connections at once, so less manual switching between endpoint needs to been
# the free tier of ngrok allows 4 connections, so it's limited to that amount
# the basic tier (or higher) is needed for http connections!
#
if [ -n "$@" ]; then
  exec "$@"
fi

# Legacy compatible:
if [ -z "$NGROK_PORT" ]; then
  if [ -n "$HTTPS_PORT" ]; then
    NGROK_PORT="$HTTPS_PORT"
  elif [ -n "$HTTPS_PORT" ]; then
    NGROK_PORT="$HTTP_PORT"
  elif [ -n "$APP_PORT" ]; then
    NGROK_PORT="$APP_PORT"
  fi
fi

ARGS="ngrok"

# Check if alternate config is supplied
if [ -z "$NGROK_CONFIG" ]; then
  NGROK_CONFIG="/home/ngrok/.ngrok2/ngrok.yml"
fi

# Set the authorization token.
if [ -n "$NGROK_AUTH" ]; then
  echo "authtoken: $NGROK_AUTH" >> $NGROK_CONFIG
fi

# first entry of multiple connection detected, fill the config file
if [ -n "$NGROK_TUNNELS" ]; then
  # Set a custom region
  if [ -n "$NGROK_REGION" ]; then
    echo "region: $NGROK_REGION " >> $NGROK_CONFIG
  fi
  echo "tunnels:" >> $NGROK_CONFIG
  echo "  $NGROK_CONN1_NAME:" >> $NGROK_CONFIG
  echo "    proto: $NGROK_CONN1_PROTO" >> $NGROK_CONFIG
  echo "    addr: $NGROK_CONN1_ADDR" >> $NGROK_CONFIG
  if [ -n "$NGROK_CONN1_HOST" ]; then
    echo "    hostname: $NGROK_CONN1_HOST" >> $NGROK_CONFIG
  fi
  if [ -n "$NGROK_CONN1_AUTH" ]; then
    echo $"    auth: \"$NGROK_CONN1_AUTH\" " >> $NGROK_CONFIG
  fi

  if [ -n "$NGROK_CONN2_NAME" ]; then
    echo "  $NGROK_CONN2_NAME:" >> $NGROK_CONFIG
    echo "    proto: $NGROK_CONN2_PROTO" >> $NGROK_CONFIG
    echo "    addr: $NGROK_CONN2_ADDR" >> $NGROK_CONFIG
    if [ -n "$NGROK_CONN2_HOST" ]; then
      echo "    hostname: $NGROK_CONN2_HOST" >> $NGROK_CONFIG
    fi
    if [ -n "$NGROK_CONN2_AUTH" ]; then
      echo $"    auth: \"$NGROK_CONN2_AUTH\" " >> $NGROK_CONFIG
    fi
  fi

  if [ -n "$NGROK_CONN3_NAME" ]; then
    echo "  $NGROK_CONN3_NAME:" >> $NGROK_CONFIG
    echo "    proto: $NGROK_CONN3_PROTO" >> $NGROK_CONFIG
    echo "    addr: $NGROK_CONN3_ADDR" >> $NGROK_CONFIG
    if [ -n "$NGROK_CONN3_HOST" ]; then
      echo "    hostname: $NGROK_CONN3_HOST" >> $NGROK_CONFIG
    fi
    if [ -n "$NGROK_CONN3_AUTH" ]; then
      echo $"    auth: \"$NGROK_CONN3_AUTH\" " >> $NGROK_CONFIG
    fi
  fi

  if [ -n "$NGROK_CONN4_NAME" ]; then
        echo "  $NGROK_CONN4_NAME:" >> $NGROK_CONFIG
        echo "    proto: $NGROK_CONN4_PROTO" >> $NGROK_CONFIG
        echo "    addr: $NGROK_CONN4_ADDR" >> $NGROK_CONFIG
    if [ -n "$NGROK_CONN4_HOST" ]; then
      echo "    hostname: $NGROK_CONN4_HOST" >> $NGROK_CONFIG
    fi
    if [ -n "$NGROK_CONN4_AUTH" ]; then
      echo $"    auth: \"$NGROK_CONN4_AUTH\" " >> $NGROK_CONFIG
    fi
  fi

  # if multple connections were configured, they all will be started
  ARGS="$ARGS start -config $NGROK_CONFIG $NGROK_TUNNELS"

else

  # Set the protocol.
  if [ "$NGROK_PROTOCOL" = "TCP" ]; then
    ARGS="$ARGS tcp"
  else
    ARGS="$ARGS http"
    NGROK_PORT="${NGROK_PORT:-80}"
  fi

  # the user has choosen a single connection, no config file will be used
  # Set a custom region
  if [ -n "$NGROK_REGION" ]; then
    ARGS="$ARGS -region=$NGROK_REGION "
  fi

  # Set the TLS binding flag
  if [ -n "$NGROK_BINDTLS" ]; then
    ARGS="$ARGS -bind-tls=$NGROK_BINDTLS "
  fi

  # Set the subdomain or hostname, depending on which is set
  if [ -n "$NGROK_HOSTNAME" ] && [ -n "$NGROK_AUTH" ]; then
    ARGS="$ARGS -hostname=$NGROK_HOSTNAME "
  elif [ -n "$NGROK_SUBDOMAIN" ] && [ -n "$NGROK_AUTH" ]; then
    ARGS="$ARGS -subdomain=$NGROK_SUBDOMAIN "
  elif [ -n "$NGROK_HOSTNAME" ] || [ -n "$NGROK_SUBDOMAIN" ]; then
    if [ -z "$NGROK_AUTH" ]; then
      echo "You must specify an authentication token after registering at https://ngrok.com to use custom domains."
      exit 1
    fi
  fi

  # Set the remote-addr if specified
  if [ -n "$NGROK_REMOTE_ADDR" ]; then
    if [ -z "$NGROK_AUTH" ]; then
      echo "You must specify an authentication token after registering at https://ngrok.com to use reserved ip addresses."
      exit 1
    fi
    ARGS="$ARGS -remote-addr=$NGROK_REMOTE_ADDR "
  fi

  if [ -n "$NGROK_HEADER" ]; then
    ARGS="$ARGS -host-header=$NGROK_HEADER "
  fi

  if [ -n "$NGROK_USERNAME" ] && [ -n "$NGROK_PASSWORD" ] && [ -n "$NGROK_AUTH" ]; then
    ARGS="$ARGS -auth=$NGROK_USERNAME:$NGROK_PASSWORD "
  elif [ -n "$NGROK_USERNAME" ] || [ -n "$NGROK_PASSWORD" ]; then
    if [ -z "$NGROK_AUTH" ]; then
      echo "You must specify a username, password, and Ngrok authentication token to use the custom HTTP authentication."
      echo "Sign up for an authentication token at https://ngrok.com"
      exit 1
    fi
  fi

  # Set the port.
  if [ -z "$NGROK_PORT" ]; then
    echo "You must specify a NGROK_PORT to expose."
    exit 1
  fi

  if [ -n "$NGROK_LOOK_DOMAIN" ]; then
    ARGS="$ARGS `echo $NGROK_LOOK_DOMAIN:$NGROK_PORT | sed 's|^tcp://||'`"
  else
    ARGS="$ARGS `echo $NGROK_PORT | sed 's|^tcp://||'`"
  fi

fi

if [ -n "$NGROK_DEBUG" ]; then
    ARGS="$ARGS -log stdout -log-level debug"
fi

set -x
exec $ARGS
