# Docker-ngRok

Docker buildfile to create a container which is capable to use all 4 connections which come with the free tier of ngRok, reducing the need to switch connections (eg. http and ssh at the same time).

The container can handle 2 config option in Azure IOT HUB:

{
  "env": [
    "NGROK_AUTH=3vKyoQ9X7w9cJk5zsWiTk",
    "NGROK_DEBUG=true",
    "NGROK_REGION=eu",
    "NGROK_TUNNELS=--all",
    "NGROK_CONN1_NAME=one",
    "NGROK_CONN1_PROTO=tcp",
    "NGROK_CONN1_ADDR=192.168.0.119:2222",
    "NGROK_CONN2_NAME=two",
    "NGROK_CONN2_PROTO=http",
    "NGROK_CONN2_ADDR=192.168.0.106:1880",
    "NGROK_CONN3_NAME=three",
    "NGROK_CONN3_PROTO=tcp",
    "NGROK_CONN3_ADDR=192.168.0.106:22",
    "NGROK_CONN4_NAME=four",
    "NGROK_CONN4_PROTO=tcp",
    "NGROK_CONN4_ADDR=192.168.0.106:22"
  ],
  "HostConfig": {
    "PortBindings": {
      "4040/tcp": [
        {
          "HostPort": "4040"
        }
      ]
    }
  }
}

All 4 connection can be online IF they are TCP connection. HTTP connections always come with 2 tunnels (HTTP and HTTPS). So only 2 concurrent HTTP connection can be setup.

The old single connection is still possible for backward comptability:

{
  "env": [
		"NGROK_PORT=22",
		"NGROK_REGION=eu",
		"NGROK_AUTH=6FRBHUm",
		"NGROK_PROTOCOL=TCP"
],
  "HostConfig": {
    "PortBindings": {
      "4040/tcp": [
        {
          "HostPort": "4040"
        }
      ]
    }
  }
}

see the original on https://hub.docker.com/r/wernight/ngrok

